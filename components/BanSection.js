import banFirst from "../images/ban_item10.jpg";
import banSecond from "../images/ban_item11.jpg";

const BanSection = () => (
<div className="wrapper">
    <div className="section_ban">
        <div className="ban_item sec1">
            <img src={banFirst} alt="" />
            <div className="sec_inner_content sec_right">
                <h5 className="sec_title text-white">DOSE OF POSITIVITY.</h5>
                <p className="text-white">Join us for Mood Boost: our mission to boost mental health through music.</p>
                <div className="sec_btn">
                    <a href="" className="" target="">Experience Mood Boost</a>
                </div>
            </div>
        </div>
    </div>
    <div className="section_ban">
        <div className="ban_item sec1">
            <img src={banSecond} alt="" />
            <div className="sec_inner_content sec_left">
                <h5 className="sec_title text-white">DOING WELL BY DOING GOOD.</h5>
                <p className="text-white">Learn more about our charitable partnerships.</p>
                <div className="sec_btn">
                    <a href="" className="" target="">Learn More</a>
                </div>
            </div>
        </div>
    </div>
    <style jsx>{`
    .ban_item{
        flex-basis: 100%;
        max-width: 100%;
    }
    .sec_right{
        transform: translate(50%, 10%);
    }
    .sec_left{
        transform: translate(-30%, 10%);
    }
    @media (max-width:640px) {  
        .sec_right{
            transform: translate(50%,-60%);
        }
        .sec_left{
            transform: translate(-30%, -50%);
        } 
    }  
    `}</style>
</div>
);

export default BanSection