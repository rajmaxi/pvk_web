import Link from 'next/link';

const navigation = [
  {
    "id" : "1",
    "name" : "Shop",
    "categorylink" :'shop'
  },
  {
    "id" : "2",
    "name" : "Moodbost",
    "categorylink" : 'moodbost'
  },
  {
    "id" : "3",
    "name" : "Inside Skullcandy",
    "categorylink" : 'insideskullcandy'
  },
  {
    "id" : "4",
    "name" : "Deals",
    "categorylink" : 'deals'
  },
  {
    "id" : "5",
    "name" : "Newarrivals",
    "categorylink" : 'newarrivals'
  },
  {
    "id" : "6",
    "name" : "Dearcollection",
    "categorylink" : 'dearcollection'
  }
]

const profiles=[
  {
    "id" : "1",
    "src" : require('../images/login.png'),
    "name": "login-ecom",
    "title": "login"
  },
  {
  "id" : "2",
  "src" : require('../images/search.png'),
  "name": "search-ecom",
  "title": "search"
  },
  {
  "id" : "3",
  "src" : require('../images/cart.png'),
  "name": "cart-ecom",
  "title": "cart"
  }
]

export default function Nav() {
  return (
    <div className="navigation_head">
      <div className="nav-main">
          <div className="topbar">
              Shipping daily on all worldwide orders
          </div>
          <div className="nav">
              <input type="checkbox" id="nav-check" />
              <div className="nav-header">
                  <div className="nav-title">
                      <Link href="/">
                          <a> Poorvika </a>
                      </Link>
                  </div>
              </div>
              <div className="nav-btn">
                  <label for="nav-check">
                  <span></span>
                  <span></span>
                  <span></span>
                  </label>
              </div>
              <div className="nav-links">
                {navigation.map(e => (
                    <Link as={`/${e.categorylink}`} key={e.id} href="/[navigation]"> 
                        <a className="text-white">{e.name}</a>
                    </Link>
                ))}
              </div>
              <div className="navitem">
                  {profiles.map(e => (
                    <a  key={e.id} target="_blank"><img src={e.src} alt={e.title} id={e.name} title={e.title}/></a>
                  ))}
              </div>
          </div>
          <style jsx>{`              
              .nav-main .topbar{
                  color: rgb(255, 255, 255);
                  background: #000;
                  text-align:center;
                  font-size: 12px;
                  padding: 10px;
                  line-height: 1.2;
                  font-weight: 300;
                  letter-spacing: 1px;
                  text-transform: uppercase;
                  z-index: 30000;
              } 
              .nav {
                  height: 50px;
                  width: 100%;
                  background-color: #fff;
                  position: relative;
                  padding: 0 55px;
                  border-bottom: 1px solid #dee2e6!important;
                }
                .nav > .nav-header {
                  display: inline;
                }
                .nav > .nav-header > .nav-title {
                  display: inline-block;
                  font-size: 22px;
                  color: #000;
                  padding: 10px 10px 10px 10px;
                }
                .nav > .nav-btn {
                  display: none;
                }
                .nav > .nav-links {
                  display: inline-block;
                  font-size: 15px;
                  margin-left: 150px;
                  text-align: center;
                }
                .navitem {
                  display: inline;
                  float: right;
                  margin-top: 14px;
              }
              .navitem img{
                  margin-right: 25px;
              }
                .nav > .nav-links > a {
                  display: inline-block;
                  padding: 13px 15px 13px 15px;
                  -webkit-text-decoration: none;
                  text-decoration: none;
                  color: #000;
                }
                .nav-links a.active {
                  font-weight: 500 !important;
                  border-bottom: 1px solid transparent;
                  border-bottom-color: #3d4246;
              }
                .nav > #nav-check {
                  display: none;
                }
                .navitem a, .nav label, .nav-links{
                  cursor: pointer;  
                }
                .nav-title a {
                  color: #000 !important;
                  text-decoration: none;
                }
                @media (max-width:1199px) {
                  .nav{
                    padding: 0 10px;
                  }
                  .nav .nav-links{
                    margin-left: 5px;
                  }
                  .footer_center_content a{
                      font-size: 12px;   
                  }
                }
                @media (max-width:992px) {
                  .nav-title{
                    font-size: 20px;
                  }
                  .nav-links a{
                      font-size: 12px;
                      padding: 10px !important;
                  }
                }
                @media (max-width:768px) {
                  .nav > .nav-btn {
                    display: inline-block;
                    position: absolute;
                    right: 0px;
                    top: 0px;
                  }
                  .nav > .nav-btn > label {
                    display: inline-block;
                    width: 50px;
                    height: 50px;
                    padding: 13px;
                  }
                  .nav > .nav-btn > label > span {
                    display: block;
                    width: 25px;
                    height: 10px;
                    border-top: 2px solid #191919d9;
                  }
                  .nav > .nav-links {
                    position: absolute;
                    display: block;
                    width: 100%;
                    background-color: #0c0a0a;
                    height: 0px;
                    transition: all 0.3s ease-in;
                    overflow-y: hidden;
                    top: 50px;
                    left: 0px;
                    z-index: 9999;
                    margin: auto;
                  }
                  .nav > .nav-links > a {
                    display: block;
                    width: 100%;
                  }
                  .nav > #nav-check:not(:checked) ~ .nav-links {
                    height: 0px;
                  }
                  .nav > #nav-check:checked ~ .nav-links {
                    height: calc(100vh - 50px);
                    overflow-y: auto;
                  }
                  .text-white{
                      color: var(--color-white) !important;
                  }
                  .nav{
                      padding: 0 40px;
                  }
                  .navitem{
                      display: none;
                  }
                  .nav label{
                    margin-right: 20px;
                  }
                }
          `}</style>
      </div>
    </div>
  )
}