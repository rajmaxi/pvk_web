import banFirst from "../images/ban_item1.jpg";
import banSecond from "../images/ban_item2.jpg";
import banThird from "../images/ban_item3.jpg";

const Banner = () => (
<div className="wrapper">
    <div className="section_ban">
        <div className="ban_item sec1">
            <img src={banFirst} alt="" />
            <div className="sec_inner_content">
            <h5 className="sec_title text-white">ZERO BOUNDARIES. MAX PERFORMANCE.</h5>
            <p className="text-white">Meet Push Ultra, feature-packed earbuds for your most fearless adventures.</p>
            <div className="sec_btn">
                <a href="" className="" target="">SHOP NOW</a>
            </div>
        </div>
        </div>
        <div className="ban_item sec2">
            <img src={banSecond} alt="" />
            <img src={banThird} alt="" />
        </div>
    </div>
    <style jsx>{`
     .ban_item.sec1 {
        flex-basis: 70%;
        max-width: 70%;
    }
    .ban_item.sec2 {
        flex-direction: column;
        flex-basis: 30%;
        max-width: 30%;
    } 
    @media (max-width:600px) {
        .sec_inner_content{
            height: 100%;
        }
    }
    `}</style>
</div>
);

export default Banner