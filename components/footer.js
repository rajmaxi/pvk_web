import insta from "../images/insta.png";
import youtube from "../images/youtube.png";
import facebook from "../images/facebook.png";
import twitter from "../images/twitter.png";


const Footer = () => (
<div className="wrapper footer-main">
    <div className="section_ban">
        <div className="ban_item sec1 sec_inner_content">
            <p>Hear It First</p>
            <input className="" id="email" name="email" type="email" value="" placeholder="Email Address"></input>
            <button className="footer-email-submit" type="submit">
                <span className="footer-email-cta"></span>
            </button>
        </div>
        <div className="ban_item sec2">
            <div className="footer_center_content footer_title foot_sup">
                <h4 className="accordion-header">Support</h4>
                <ul className="footer-info-list">
                    <li>
                        <a href="">Help Center</a>
                    </li>
                    <li>
                        <a href="">Contact Us</a>
                    </li>
                    <li>
                        <a href="">Account</a>
                    </li>
                    <li>
                        <a href="">Product Help</a>
                    </li>
                    <li>
                        <a href="">Warranty</a>
                    </li>
                </ul>
            </div>
            <div className="footer_center_content footer_title foot_off">
                <h4 className="accordion-header">Offer</h4>
                <ul className="footer-info-list">
                    <li>
                        <a href="">Compare</a>
                    </li>
                    <li>
                        <a href="">Bulk Orders</a>
                    </li>
                    <li>
                        <a href="">Protect Our</a>
                    </li>
                    <li>
                        <a href="">Winters</a>
                    </li>
                </ul>
            </div>
            <div className="footer_center_content footer_title foot_abo">
                <h4 className="accordion-header">Aboutus</h4>
                <ul className="footer-info-list">
                    <li>
                        <a href="">About</a>
                    </li>
                    <li>
                        <a href="">Athletes</a>
                    </li>
                    <li>
                        <a href="">Careers</a>
                    </li>
                </ul>
            </div>
        </div>
        <div className="ban_item sec3 social_apps">
                <p>Follow Us</p>
                <div className="footer_center_content">
                    <ul className="footer-info footer-info-list">
                        <li>
                            <a href=""><img src={insta} alt="instagram" title="Instagram" /></a>
                        </li>
                        <li>
                            <a href=""><img src={youtube} alt="youtube" title="Youtube" /></a>
                        </li>
                        <li>
                            <a href=""><img src={facebook} alt="facebook" title="Facebook" /></a>
                        </li>
                        <li>
                            <a href=""><img src={twitter} alt="twitter" title="Twitter" /></a>
                        </li>
                    </ul>
                </div>
        </div>
    </div>
    <div className="footer_bottom">
        <div className="footer_privacy">
            <p>Privacy Policy | Terms of use</p>
        </div>
        <div className="footer_rights">
            <p>© 2020 Poorvika.eu All Rights Reserved </p>
        </div>
    </div>
    <style jsx>{`
        .footer-main{
            background: #000;
            border-top: 1px solid #383838;
        }
        .footer_bottom{
            background: #000;   
        }
        .section_ban{
            border-bottom: 1px solid #383838;
        }
        .footer_privacy, .footer_rights{
            flex-basis: 50%;
            max-width: 50%;
            padding:10px;
        }
        .ban_item{
            flex-basis: 33.3%;
            max-width: 33.3%;
            padding: 60px 0px;
        }
        .ban_item::after{
            content: "";
            position: absolute;
            top: 0;
            width: 100%;
            transform: rotate(65deg);
            transform-origin: 0;
            border-top: 1px solid #2c2c2d;
            left: 85%;
        }
        .sec_inner_content{
            flex-direction: column;
        }
        .sec_inner_content p , .social_apps p{
            font-size: 18px;
            color: #fff;
            text-transform: uppercase;
            text-align: center;
        }
        .sec1.sec_inner_content {
            margin: 0 auto;
            text-align: center;
            display: block;
        }
        #email{
            border-radius: 0;
            background-color: #f6f6f6;
            border-style: solid;
            border-width: 1px;
            border-color: #949494;
            box-shadow: none;
            color: #383838;
            display: block;
            font-family: inherit;
            font-size: 14px;
            height: 3.26107rem;
            padding: .7775rem;
            width: 100%;
            box-sizing: border-box;
            margin: auto;
            max-width: 350px;
        }
        #email:focus, #email:hover {
            outline: 0;
            border: 0;
        }
        .footer-email-submit {
            position: absolute;
            top: 60px;
            right: 60px;
            width: 50px;
            height: 50px;
            padding: 0;
            background: 0 0!important;
            outline: 0;
            border: 0;
        }
        .footer-email-cta {
            display: block;
            width: 50px;
            height: 50px;
        }
        .footer-email-cta::before {
            content: "";
            position: absolute;
            top: 50%;
            left: 40%;
            width: 14px;
            height: 14px;
            transform: translateY(-50%) rotate(45deg);
            border-top: 1px solid #000;
            border-right: 1px solid #000;
        }
        .footer_center_content {
            margin-right: 20px;
        }
        ul.footer-info-list li {
            list-style: none;
            margin-bottom: 10px;
        }
        ul.footer-info-list li a , .footer_bottom p{
            text-decoration: none;
            color: #fff;
            font-size: 11px;
        }
        .social_apps img{
            width: 28px;
            height: 28px;
        }
        .footer-info {
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            align-items: center;
            justify-content: space-evenly;
            min-width: 300px;
            width: 300px;
            margin: auto;
            padding: 0;
        }
        .footer_privacy{
            margin-left: 40px;
        }
        .footer_rights{
            text-align: right;
            margin-right: 40px;
        }
        .accordion-header{
            display: none;
        }
        @media (max-width:1199px) {
            #email{
                max-width: 300px;
            }
            .footer-email-submit{
                top: 120px;
                right: 15px;
            }
            .footer-email-cta{
                display: block;
                width: 35px;
                height: 35px;
            }
        }
        @media (max-width:992px) {
        .footer-main .ban_item.sec1, .footer-main .ban_item.sec2{
            flex-basis: 50%;
            max-width: 50%;
            padding: 0 30px;
          }
          .footer-main .ban_item.sec3 {
            padding: 0 30px;
            flex-basis: 100%;
            max-width: 100%;
          }
        }
        @media (max-width:768px) {
            .footer-main .ban_item{
                flex-basis: 100% !important;
                max-width: 100% !important;
            }
            .footer-info-list{
                padding: 0;
                margin: 30px auto;
                width: 200px;
                max-width: 200px;
                padding-left: 20px;
            }
            .footer-info-list a {
              font-size: 12px;
            }
        }
        @media (max-width:600px) {    
            .footer-info-list {
                width: 150px;
                max-width: 150px;
                padding-left: 10px;
            }
            .footer_privacy p {
                font-size: 12px;
            }
            .accordion-header{
                display: block;
            }
            .footer-info-list{
                display: none;
            }
            .footer_center_content.footer_title h4{
                display: block;
                color: #c5baba;
                font-size: 12px;
                text-transform: uppercase;
                line-height: 1;
                padding: 15px;
            }
            .footer_center_content.footer_title:after {
                content: "";
                position: absolute;
                left: 0;
                right: 0;
                bottom: 0;
                width: 100%;
                height: 1px;
                background: #7b7b7b96;
            }
            .footer-main .ban_item {
                display: block;
            }
            .foot_sup:after{
                top: 30%;
            }
            .foot_off:after{
                top: 60%;
            }
            .foot_abo:after{
                top: 90%;
            }
            .section_ban .ban_item {
                padding: 15px 30px !important;
            }
            .footer_center_content h4:before, .footer_center_content h4:after{
                content: "";
                position: absolute;
                top: 20%;
                right: 2.14286rem;
                width: 1px;
                height: 14px;
                background-color: #cecece;
                margin-top: -7px;
                transition: all .3s ease-out;
            }
            .footer_center_content h4:before {
                transform: rotate(90deg);
            }
            .foot_sup h4:before, .foot_sup h4:after{
                top: 25%;
            }
            .foot_off h4:before, .foot_off h4:after{
                top: 50%;
            }
            .foot_abo h4:before, .foot_abo h4:after{
                top: 75%;
            }
            .ban_item::after{
                border: none;
            }
        }
    `}</style>
</div>
);

export default Footer