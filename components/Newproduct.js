import banFirst from "../images/ban_item5.jpg";
import banSecond from "../images/ban_item4.jpg";
import banThird from "../images/ban_item6.jpg";
import banFour from "../images/ban_item7.jpg";
import banFive from "../images/ban_item8.jpg";
import banSix from "../images/ban_item9.jpg";

const Newproduct = () => (
<div className="wrapper">
    <div className="section_ban">
        <div className="ban_item sec1">
            <img src={banFirst} alt="" />
            <a className="hover_img"><img src={banFour} alt="" /></a>
        </div>
        <div className="ban_item sec2">
            <img src={banSecond} alt="" />
            <a className="hover_img"><img src={banFive} alt="" /></a>
        </div>
        <div className="ban_item sec3">
            <img className="height_img" src={banThird} alt="" />
            <a className="hover_img"><img src={banSix} alt="" /></a>
        </div>
    </div>
    <style jsx>{`
        .height_img {
            height: 100%;
        }
         .ban_item{
            flex-basis: 33.3%;
            max-width: 33.3%;
        }
        .ban_item a.hover_img {
            cursor: pointer;
            position: absolute;
            z-index: 9999;
            opacity: 0;
            transition: opacity .3s ease-out .1s;
        }
        .ban_item a.hover_img:hover {
            opacity: 1;
        }
        @media (max-width:640px) {   
            .sec_inner_content{
                width: 65%;
                height: 100%;
            }
        }
    `}</style>
</div>
);

export default Newproduct 