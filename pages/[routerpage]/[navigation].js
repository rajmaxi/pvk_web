import { useRouter } from 'next/router';

export default function Navigation() {
    const router = useRouter();
    console.log(router.query);
    return <h2>{router.query.navigation}'s {router.query.routerpage}</h2>
}