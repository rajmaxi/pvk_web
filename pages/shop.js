import Head from 'next/head';
import fetch from 'isomorphic-unfetch';
import Link from 'next/link';
import Nav from '../components/nav';
import Footer from '../components/footer';

export default function Shop(props) {
  // console.log('data1',props.res)
    return <div className="container">
        <Head>
            <title>Poorvika Ecommerce</title>
            <link rel="icon" href="/favicon.ico" />
         </Head>
         <Nav />
        <div className="wrapper">
            <h1 className="page-heading">Shop</h1>
            <div className="product_category">
                <ul className="category_item">
                    {props.res.breadcrumb.map((e,i)=>{
                        return <li className='{e.crumblink} category_iteminner' key={`some-id${i}`}>{e.name}</li>
                    })}
                    <li className="sort_item">
                    Sort: 
                    <select className="select-filter">
                        <option selected>Featured Items</option>
                        <option>Newest Items</option>
                        <option>Best Selling</option>
                        <option>A to Z</option>
                        <option>Z to A</option>
                    </select>
                    </li>
                </ul>
            </div>
            <div className="product_list">
                {props.res.productlist.map((e,i)=>{
                    return <div className="list_item" key={`some-id${i}`}>
                        <div className="product_image">
                            <img src={e.pics}/>
                        </div>
                        <div className="thumb_main">
                            {e.thumbimg.map((thumb,i) =>{
                                return  <div className="thumb_img" key={`thumb-${i}`}>
                                            <img src={thumb.thumbsrc}/>
                                        </div>
                            })}
                        </div>
                        <div className="name">
                            <h4>{e.name}</h4>
                        </div>
                        <div className="price">
                            <h4>{e.price}</h4>
                        </div>
                    </div>
                })}
            </div>
        </div>
        <Footer/>
        <style jsx global>{`
              html,
              body {
                padding: 0;
                margin: 0;
                font-family: "Montserrat","Helvetica Neue",Arial,sans-serif;
              }
              * {
                box-sizing: border-box;
              }
              :root {  
                --color-white: #fff;
              }
              .text-white{
                color: var(--color-white);
              }
              .section_ban, .ban_item.sec1, .ban_item.sec2, .footer_bottom{
                display: -webkit-flex;
                display: -ms-flexbox;
                display: flex;
                position: relative;
            }
            .section_ban{
                flex-wrap: wrap;
                overflow: hidden;
                width: 100%;
                height: 100%; 
            }
            .ban_item img{
                position: relative;
                width: 100%;
                height: auto;
                cursor: pointer;
            }
            .sec_inner_content {
              position: absolute;
              top: 0;
              left: 0;
              bottom: 0;
              right: 0;
              width: 50%;
              height: 50%;
              margin: auto;
          }
          .sec_btn a{
              padding: 15px 20px;
              position: relative;
              overflow: hidden;
              height: 3.57143rem;
              line-height: 3.57143rem;
              transition: all .15s ease;
              font-family: HelveticaNeueW02-75Bold,HelveticaNeueBold,HelveticaNeue-Bold,"Helvetica Neue Bold",HelveticaNeue,"Helvetica Neue",TeXGyreHerosBold,Helvetica,Tahoma,Geneva,Arial,sans-serif;
              letter-spacing: .07143rem;
              font-size: 12px;
              text-transform: uppercase;
              border: 0;
              white-space: nowrap;
              text-align: center;
              z-index: 98;
              color: #000;
              background: #f6f6f6;
              text-decoration: none;
          } 
          @media (min-width:1366px) {
              .sec_inner_content h5 , .sec_inner_content p {
                  white-space: pre-line;
              }
              .sec_inner_content h5{
                  font-size: 50px;
                  margin: 0;
              }
              .sec_inner_content p {
                  font-size: 30px;
              }
              .sec_btn a{
                  font-size: 18px;
              }
            }
            @media (max-width:480px) {
              .sec_btn a{
                display: none;
              }
              .sec_inner_content p {
                font-size: 12px;
              }
            }
        `}</style>
         <style jsx>{`
            .page-heading {
                font-size: 3.1em;
                text-transform: uppercase;
                text-align: center;
                font-weight: 700;
                padding: 2rem 0rem 1rem 0rem;
                margin: 0;
            }
            .product_list{
                display: flex;
                flex-wrap: wrap;
            }
            .list_item{
                flex-basis:33.3%;
                max-width: 33.3%;
                margin-bottom: 4rem;
                display: flex;
                flex-direction: column;
                align-items: center;
                justify-content: center;
            }
            .thumb_main {
                display: flex;
                align-items: center;
                justify-content: center;
            }
            .wrapper{
                margin: 0 auto;
            }
            .wrapper img{
                width: 100%;
                height: auto;
                max-width: 280px;
                margin: 0 auto;
                padding-bottom: 25px;
            }
            .thumb_img {
                width: 60px;
                height: 60px;
                padding-right: 20px;
            }
            .category_item {
                display: flex;
                flex-wrap: nowrap;
                list-style: none;
                justify-content: space-between;
                align-items: center;
                padding: 0;
                margin: 2rem 0px 3rem;
            }
            .category_item li {
                max-width: 19%;
                flex-basis: 19%;
                font-size: 13px;
                text-transform: uppercase;
            }
            .category_iteminner{
                height: 40px;
                line-height: 40px;
                letter-spacing: .07143rem;
                font-size: 13px;
                text-transform: uppercase;
                border: 0;
                white-space: nowrap;
                text-align: center;
                background: #ededed;
                z-index: 98;
            }  
            select.select-filter {
                margin-left: 15px;
                height: 40px;
                line-height: 40px;
                padding: 5px;
                font-size: 12px;
                max-width: 150px;
                width: 150px;
                border: 1px solid #dee2e6;
                text-transform: uppercase;
            }
            .list_item h4 {
                font-size: 15px;
                white-space: pre-line;
                text-align: center;
                max-width: 300px;
                width: 300px;
                font-weight: 500;
                color: #0e0e0e;
                margin: 1em 0px 0em;
            }
            @media (min-width: 576px) {
                .wrapper {
                    max-width: 540px;
                }
            }
            @media (min-width: 768px) {
                .wrapper {
                    max-width: 720px;
                }
            }
            @media (min-width: 992px) {
                .wrapper {
                    max-width: 960px;
                }
            }
            @media (min-width: 1200px) {
                .wrapper {
                    max-width: 1140px;
                }
            }
         `}</style>
    </div>
}

Shop.getInitialProps = async () => {
  const response = await fetch('https://raw.githubusercontent.com/rajmaxi/json_server/master/db.json');
  const data = await response.json();
  return {
     res:data
    }
}