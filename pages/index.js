import Head from 'next/head';
import Nav from '../components/nav';
import Banner from '../components/Banner';
import Newproduct from '../components/Newproduct';
import BanSection from '../components/BanSection';
import Footer from '../components/footer';

export default function Home() {
  return (
    <div className="container">
      <Head>
        <title>Poorvika Ecommerce</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <div className="home_content">
        <Nav />
        <Banner />
        <Newproduct />
        <BanSection />
        <Footer />
      </div>
      
      <style jsx global>{`
        html,
          body {
          padding: 0;
          margin: 0;
          font-family: "Montserrat","Helvetica Neue",Arial,sans-serif;
        }
        * {
          box-sizing: border-box;
        }
        :root {  
          --color-white: #fff;
        }
        .section_ban, .ban_item.sec1, .ban_item.sec2, .footer_bottom{
          display: -webkit-flex;
          display: -ms-flexbox;
          display: flex;
          position: relative;
      }
      .section_ban{
          flex-wrap: wrap;
          overflow: hidden;
          width: 100%;
          height: 100%; 
      }
      .ban_item img{
          position: relative;
          width: 100%;
          height: auto;
          cursor: pointer;
      }
      .sec_inner_content {
        position: absolute;
        top: 0;
        left: 0;
        bottom: 0;
        right: 0;
        width: 50%;
        height: 50%;
        margin: auto;
    }
    .sec_btn a{
        padding: 15px 20px;
        position: relative;
        overflow: hidden;
        height: 3.57143rem;
        line-height: 3.57143rem;
        transition: all .15s ease;
        font-family: HelveticaNeueW02-75Bold,HelveticaNeueBold,HelveticaNeue-Bold,"Helvetica Neue Bold",HelveticaNeue,"Helvetica Neue",TeXGyreHerosBold,Helvetica,Tahoma,Geneva,Arial,sans-serif;
        letter-spacing: .07143rem;
        font-size: 12px;
        text-transform: uppercase;
        border: 0;
        white-space: nowrap;
        text-align: center;
        z-index: 98;
        color: #000;
        background: #f6f6f6;
        text-decoration: none;
    } 
    @media (min-width:1366px) {
        .sec_inner_content h5 , .sec_inner_content p {
            white-space: pre-line;
        }
        .sec_inner_content h5{
            font-size: 50px;
            margin: 0;
        }
        .sec_inner_content p {
            font-size: 30px;
        }
        .sec_btn a{
            font-size: 18px;
        }
      }
      @media (max-width:480px) {
        .sec_btn a{
          display: none;
        }
        .sec_inner_content p {
          font-size: 12px;
        }
      }
      `}</style>
    </div>
  )
}

